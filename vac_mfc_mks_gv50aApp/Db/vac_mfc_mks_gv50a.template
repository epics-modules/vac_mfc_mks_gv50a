################################################################################
# Database for mass flow controller for
# MKS946 Vacuum Mass Flow & Gauge Controller
################################################################################

record(fanout, "${CONTROLLERNAME}:${CHANNEL}:iInit-FO") {
    field(LNK1, "${DEVICENAME}:ScaleF-RB")
    field(LNK2, "${DEVICENAME}:NomRng-RB")
    field(LNK3, "${DEVICENAME}:FlwSP-RB")
    field(LNK4, "${DEVICENAME}:Mode-RB")
}

alias("${CONTROLLERNAME}:CommsOK",                "${DEVICENAME}:CommsOK")
alias("${CONTROLLERNAME}:${CHANNEL}:ChanR",       "${DEVICENAME}:ChanR")
alias("${CONTROLLERNAME}:${CHANNEL}:PrsR",        "${DEVICENAME}:FlwR")
alias("${CONTROLLERNAME}:${CHANNEL}:PrsR-STR",    "${DEVICENAME}:FlwR-STR")
alias("${CONTROLLERNAME}:${CHANNEL}:PrsStatR",    "${DEVICENAME}:PrsStatR")
alias("${CONTROLLERNAME}:${CHANNEL}:SensorTypeR", "${DEVICENAME}:SensorTypeR")
alias("${CONTROLLERNAME}:${CHANNEL}:ValidR",      "${DEVICENAME}:ValidR")
alias("${CONTROLLERNAME}:${CHANNEL}:iCheckType",  "${DEVICENAME}:iCheckType")
alias("${CONTROLLERNAME}:${CHANNEL}:NumOfRlysR",  "${DEVICENAME}:NumOfRlysR")

#FIXME: I have no idea if 'FC' is the two letter code for mass flow controller
record("*", "${DEVICENAME}:iCheckType") {
    field(CALC, "(A&&AA=='FC')?'1':'0'")
}

record("*", "${CONTROLLERNAME}:${CHANNEL}:DevNameR") {
    field(PINI, "YES")
    field(VAL,  "${DEVICENAME}")
}

record("*", "${CONTROLLERNAME}:${CHANNEL}:NumOfRlysR") {
    field(VAL,  "2")
    field(PINI, "YES")
}

#FlwR is an alias to PrsR and PrsR is set to mbar, let's fix that
record(stringout, "${DEVICENAME}:iFlwEGU") {
    field(DESC, "Fix flow unit")
    field(PINI, "YES")
    field(VAL,  "sccm")
    field(OUT,  "${DEVICENAME}:FlwR.EGU")
}

record(ai, "${DEVICENAME}:ScaleF-RB") {
    field(DESC, "Get scale factor")
    field(DTYP, "stream")
    field(INP,  "@vac_mfc_mks_gv50a.proto get_scale_factor(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

record(ao, "${DEVICENAME}:ScaleFS") {
    field(DESC, "Set scale factor")
    field(DTYP, "stream")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_scale_factor(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(DRVL, "0.1")
    field(LOPR, "0.1")
    field(DRVH, "50")
    field(HOPR, "50")
    field(FLNK, "${DEVICENAME}:ScaleF-RB")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

record(ai, "${DEVICENAME}:NomRng-RB") {
    field(DESC, "Get full scale nominal range")
    field(DTYP, "stream")
    field(EGU,  "sccm")
    field(INP,  "@vac_mfc_mks_gv50a.proto get_range(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

record(ao, "${DEVICENAME}:NomRngS") {
    field(DESC, "Set full scale nominal range")
    field(DTYP, "stream")
    field(EGU,  "sccm")
    field(DRVH, "1000000")
    field(DRVL, "0.1")
    field(HOPR, "1000000")
    field(LOPR, "0.1")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_range(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(FLNK, "${DEVICENAME}:NomRng-RB")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

record(bo, "${DEVICENAME}:ZeroS") {
    field(DESC, "Zero the MFC")
    field(DTYP, "stream")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_zero(${SERADDR},${CHANNEL_N},${DEVICENAME}:Zero-RB) ${ASYNPORT}")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

record(bi, "${DEVICENAME}:Zero-RB") {
    field(DESC, "Result of Zero operation")
    field(ZNAM, "Failed")
    field(ONAM, "OK")
}

record(ai, "${DEVICENAME}:FlwSP-RB") {
    field(DESC, "Get flow setpoint")
    field(DTYP, "stream")
    field(EGU,  "sccm")
    field(PREC, "2")
    field(INP,  "@vac_mfc_mks_gv50a.proto get_flow_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

record(ao, "${DEVICENAME}:FlwSPS") {
    field(DESC, "Set flow setpoint")
    field(DTYP, "stream")
    field(EGU,  "sccm")
    field(PREC, "2")
    field(HOPR, "1000")
    field(LOPR, "0")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_flow_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(FLNK, "${DEVICENAME}:FlwSP-RB")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

record(bi, "${DEVICENAME}:iCntTimElapsed") {
    field(DESC, "If time should be tracked")
    field(PINI, "YES")
    field(VAL,  "0")
}

record(longout, "${DEVICENAME}:FlwInRngTimeoS") {
    field(DESC, "Time to reach 10% of FlwSP-RB")
    field(EGU,  "sec")
    field(PINI, "YES")
    field(DRVL, "1")
    field(DRVH, "100")
    field(VAL,  "10")
    field(OUT,  "${DEVICENAME}:FlwSPTimElapsedR.HIGH")
}

record(calcout, "${DEVICENAME}:iFlwSPTimElapsedR") {
    field(DESC, "Calc time to reach 10% of FlwSP-RB")
    field(INPA, "${DEVICENAME}:iFlwSPTimElapsedR")
    field(CALC, "A < 120 ? A + 1 : A")
    field(SCAN, "1 second")
    field(OUT,  "${DEVICENAME}:FlwSPTimElapsedR PP")
    field(DISV, "0")
    field(SDIS, "${DEVICENAME}:iCntTimElapsed")
}

record(longin, "${DEVICENAME}:FlwSPTimElapsedR") {
    field(DESC, "Time it took to reach 10% of FlwSP-RB")
    field(EGU,  "sec")
    field(HSV,  "MINOR")
    field(HIHI, "120")
    field(HHSV, "MAJOR")
}

record(bi, "${DEVICENAME}:FlwInRngR") {
    field(DESC, "Flow rate is within range of FlwSP-RB")
    field(ONAM, "In range")
    field(ZNAM, "Out of range")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR CP")
}

#Set by check_flow_rate.st
record(bi, "${DEVICENAME}:FlwRngTimeoR") {
    field(DESC, "Flow rate range of FlwSP-RB timeout")
    field(ONAM, "Timeout")
    field(OSV,  "MAJOR")
}

record(calcout, "${DEVICENAME}:iChkFlwInRng") {
    field(DESC, "Check if flow rate is within range")
    field(INPA, "${DEVICENAME}:FlwSP-RB CP")
    field(INPB, "${DEVICENAME}:FlwR CP")
    field(CALC, "A ? (abs(1-A/B) <= 0.1) : (B <= 0.1)")
    field(OUT,  "${DEVICENAME}:FlwInRngR PP")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

#
# WARNING
# Whenever you change / reorder values here go and update check_flow_rate.st
# The integer value of Sepoint needs to be in sync between these two and check_flow_rate.st
#
record(mbbi, "${DEVICENAME}:Mode-RB") {
    field(DESC, "Get control enabled status")
    field(DTYP, "stream")
    field(ZRST, "Closed")
    field(ONST, "Setpoint")
    field(TWST, "Open")
    field(THST, "PID Ctrl")
    field(FRST, "Ratio A")
    field(FVST, "Ratio M")
    field(INP,  "@vac_mfc_mks_gv50a.proto get_mode(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

record(mbbo, "${DEVICENAME}:ModeS") {
    field(DESC, "Set control enabled status")
    field(DTYP, "stream")
    field(ZRVL, "0")
    field(ZRST, "Close")
    field(ONVL, "1")
    field(ONST, "Setpoint")
    field(TWVL, "2")
    field(TWST, "Open")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_mode(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(FLNK, "${DEVICENAME}:Mode-RB")
    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${DEVICENAME}:ValidR")
}

#
# FlwStatR logic
#
record(calc, "${DEVICENAME}:iFlwStat-FO") {
    field(DESC, "PV to trigger UI status update")
    field(INPA, "${DEVICENAME}:PrsStatR CP")
    field(INPB, "${DEVICENAME}:Mode-RB CP")
    field(INPC, "${DEVICENAME}:FlwRngTimeoR CP")
    field(INPD, "${DEVICENAME}:FlwInRngR CP")
    field(CALC, "A")
    field(FLNK, "${DEVICENAME}:iFlwStatSeq")
}

record(seq, "${DEVICENAME}:iFlwStatSeq") {
    field(SELM, "Specified")
    field(SELL, "${DEVICENAME}:PrsStatR MSS")
    field(OFFS, "1")

# ON
    field(DOL2, "1")
    field(LNK2, "${DEVICENAME}:iModeSeq MSS PP")

# ERROR
    field(DOL4, "3")
    field(LNK4, "${DEVICENAME}:FlwStatR MSS PP")

# INVALID
    field(DOL1, "0")
    field(LNK1, "${DEVICENAME}:FlwStatR MSS PP")
# OFF
    field(DOL3, "0")
    field(LNK3, "${DEVICENAME}:FlwStatR MSS PP")
# OVER-RANGE
    field(DOL5, "0")
    field(LNK5, "${DEVICENAME}:FlwStatR MSS PP")
# UNDER-RANGE
    field(DOL6, "0")
    field(LNK6, "${DEVICENAME}:FlwStatR MSS PP")
}

record(seq, "${DEVICENAME}:iModeSeq") {
    field(DISP, "1")
    field(SELM, "Specified")
    field(SELL, "${DEVICENAME}:Mode-RB MSS")
    field(OFFS, "1")

# Closed
    field(DOL1, "2")
    field(LNK1, "${DEVICENAME}:FlwStatR MSS PP")

# Setpoint
    field(DOL2, "1")
    field(LNK2, "${DEVICENAME}:iChkFlwRngStat MSS PP")

# Open
    field(DOL3, "1")
    field(LNK3, "${DEVICENAME}:FlwStatR MSS PP")

# PID Ctrl
    field(DOL4, "0")
    field(LNK4, "${DEVICENAME}:FlwStatR MSS PP")
# Ratio A
    field(DOL5, "0")
    field(LNK5, "${DEVICENAME}:FlwStatR MSS PP")
# Ratio M
    field(DOL6, "0")
    field(LNK6, "${DEVICENAME}:FlwStatR MSS PP")
}

record(calcout, "${DEVICENAME}:iChkFlwRngStat") {
    field(DISP, "1")
# Do not MSS this one. It can be UDF until a setpoint change
    field(INPA, "${DEVICENAME}:FlwRngTimeoR")
    field(INPB, "${DEVICENAME}:FlwInRngR MSS")
    field(CALC, "A ? 3 : (B ? 1 : 0)")
    field(OUT,  "${DEVICENAME}:FlwStatR MSS PP")
}

record(mbbi, "${DEVICENAME}:FlwStatR") {
    field(ZRST, "Invalid")
    field(ONST, "Open")
    field(TWST, "Closed")
    field(THST, "Error")
    field(THSV, "MAJOR")
}
